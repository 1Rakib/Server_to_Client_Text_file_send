package com.company;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ClientFile {
    public void go() {
        try {
                Scanner sc = new Scanner(System.in);
                Socket s = new Socket("127.0.0.1", 4242);
                System.out.println(s);
                System.out.println("InetAddress: " + s.getInetAddress());
                System.out.println("LocalAddress: " + s.getLocalAddress());
                System.out.println("LocalPort :" + s.getLocalPort());

                OutputStream stream = s.getOutputStream();
                PrintWriter writer = new PrintWriter(stream);

                InputStreamReader streamReader = new InputStreamReader(s.getInputStream());
                BufferedReader reader = new BufferedReader(streamReader);

                String text = sc.nextLine();
                writer.println(text);

                System.out.println("Request sent");
                writer.flush();

                String size_str = reader.readLine();
                int i=Integer.parseInt(size_str);
            System.out.println(i);
                String str2 = "copy_text.txt";
                FileOutputStream fos = new FileOutputStream(str2);
                DataOutputStream dos = new DataOutputStream(fos);

                for(int k = 0; k<i; k++) {

                    String response = reader.readLine();
                    System.out.println("Response received");
                    System.out.println(response);
                    dos.write(Integer.parseInt(response));

                }

    /*            String response = reader.readLine();
                System.out.println("Response received");

                FileInputStream fis = new FileInputStream(response);
                DataInputStream dis = new DataInputStream(fis);

                FileOutputStream fos = new FileOutputStream("a.txt");
                DataOutputStream dos = new DataOutputStream(fos);
*/
               /* int availableBytes = 0;
                while ((availableBytes = dis.available()) != 0) {
                    byte[] byteToRead = new byte[availableBytes];
                    dis.read(byteToRead);

                    for (int i = 0; i < byteToRead.length; i++) {
                        dos.write(byteToRead[i]);
                        System.out.println(byteToRead[i]);

                    }
                }*/


                    dos.flush();

                    dos.close();
                    fos.close();

/*                    dis.close();
                    fis.close();*/



                    writer.close();

        } catch (IOException ex) {
            ex.printStackTrace();

        }
    }

    public static void main(String[] args) {
        ClientFile client = new ClientFile();
        client.go();
    }
}
